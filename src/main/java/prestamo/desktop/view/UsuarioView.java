package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JFormattedTextField;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JButton;

public class UsuarioView extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UsuarioView frame = new UsuarioView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UsuarioView() {
		setTitle("Registro de Nuevo Usuario");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 336, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(142, 173, 137, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNombre = new JLabel("Nombres");
		lblNombre.setBounds(38, 66, 46, 14);
		contentPane.add(lblNombre);
		
		JLabel lblApellidos = new JLabel("Apellidos");
		lblApellidos.setBounds(38, 94, 46, 14);
		contentPane.add(lblApellidos);
		
		JLabel lblRut = new JLabel("RUT");
		lblRut.setBounds(38, 120, 46, 14);
		contentPane.add(lblRut);
		
		JLabel lblNewLabel = new JLabel("Calle");
		lblNewLabel.setBounds(38, 148, 46, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNumeroCasa = new JLabel("Numero Casa (#)");
		lblNumeroCasa.setBounds(38, 176, 94, 14);
		contentPane.add(lblNumeroCasa);
		
		JLabel lblCorreoElectrnico = new JLabel("Correo Electr\u00F3nico");
		lblCorreoElectrnico.setBounds(38, 232, 94, 14);
		contentPane.add(lblCorreoElectrnico);
		
		JLabel lblComuna = new JLabel("Comuna");
		lblComuna.setBounds(38, 204, 46, 14);
		contentPane.add(lblComuna);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(142, 229, 137, 20);
		contentPane.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(142, 145, 137, 20);
		contentPane.add(textField_2);
		
		textField_5 = new JTextField();
		textField_5.setColumns(10);
		textField_5.setBounds(142, 117, 137, 20);
		contentPane.add(textField_5);
		
		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(142, 91, 137, 20);
		contentPane.add(textField_6);
		
		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(142, 63, 137, 20);
		contentPane.add(textField_7);
		
		JLabel lblIngreseSusDatos = new JLabel("Ingrese sus Datos Personales");
		lblIngreseSusDatos.setBounds(95, 32, 148, 20);
		contentPane.add(lblIngreseSusDatos);
		
		JLabel lblTipoUsuario = new JLabel("Tipo Usuario");
		lblTipoUsuario.setBounds(38, 260, 83, 14);
		contentPane.add(lblTipoUsuario);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(142, 257, 137, 20);
		contentPane.add(comboBox_1);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setBounds(142, 201, 137, 20);
		contentPane.add(comboBox_2);
		
		JButton btnGuardar = new JButton("Cancelar");
		btnGuardar.setBounds(119, 384, 89, 23);
		contentPane.add(btnGuardar);
		
		JButton button = new JButton("Guardar");
		button.setBounds(119, 350, 89, 23);
		contentPane.add(button);
	}
}
