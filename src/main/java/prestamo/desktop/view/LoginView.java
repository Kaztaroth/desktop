package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class LoginView extends JFrame {

	private JPanel contentPane;
	private JTextField UserField;
	private JPasswordField passwordField;
	private JButton btnIniciarSesion;
	private JButton btnCrearUsuario;
	
	

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					LoginView frame = new LoginView();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public LoginView() {
		setTitle("Bienvenido");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 297, 275);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblInicieSesion = new JLabel("Ingrese sus datos \r\npara iniciar sesi�n");
		lblInicieSesion.setHorizontalAlignment(SwingConstants.CENTER);
		lblInicieSesion.setBounds(38, 11, 209, 59);
		contentPane.add(lblInicieSesion);

		JLabel lblEmail = new JLabel("Correo Electr�nico");
		lblEmail.setBounds(25, 79, 88, 14);
		contentPane.add(lblEmail);

		UserField = new JTextField();
		UserField.setBounds(136, 76, 111, 17);
		contentPane.add(UserField);
		UserField.setColumns(10);

		JLabel lblPassword = new JLabel("Contrase�a");
		lblPassword.setBounds(25, 104, 88, 14);
		contentPane.add(lblPassword);

		passwordField = new JPasswordField();
		passwordField.setBounds(136, 104, 111, 17);
		contentPane.add(passwordField);

		JButton btnIniciarSesion = new JButton("Iniciar Sesi�n");
		btnIniciarSesion.setBounds(81, 148, 119, 23);
		contentPane.add(btnIniciarSesion);

		JButton btnCrearUsuario = new JButton("Crear Usuario");
		btnCrearUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnCrearUsuario.setBounds(90, 182, 100, 23);
		contentPane.add(btnCrearUsuario);
	}

	public JButton getBtnIniciarSesion() {
		return btnIniciarSesion;
	}

	public JTextField getUserField() {
		return UserField;
	}

	public void setTextField(JTextField textField) {
		this.UserField = textField;
	}

	public JPasswordField getPasswordField() {
		return passwordField;
	}

	public void setPasswordField(JPasswordField passwordField) {
		this.passwordField = passwordField;
	}


	public JButton getBtnCrearUsuario() {
		return btnCrearUsuario;
	}

	public void setBtnCrearUsuario(JButton btnCrearUsuario) {
		this.btnCrearUsuario = btnCrearUsuario;
	}
		
			
}