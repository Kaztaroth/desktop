package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;

public class PrincipalView extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PrincipalView frame = new PrincipalView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PrincipalView() {
		setTitle("Prestamos");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 234, 332);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnSolicitarPrestamo = new JButton("Solicitar Prestamo");
		btnSolicitarPrestamo.setBounds(47, 71, 117, 23);
		contentPane.add(btnSolicitarPrestamo);
		
		JButton btnPrestarDinero = new JButton("Prestar Dinero");
		btnPrestarDinero.setBounds(47, 105, 117, 23);
		contentPane.add(btnPrestarDinero);
		
		JButton btnEstadoPrestamo = new JButton("Estado Prestamo");
		btnEstadoPrestamo.setBounds(47, 139, 117, 23);
		contentPane.add(btnEstadoPrestamo);
		
		JButton btnPagarCouta = new JButton("Pagar Cuota");
		btnPagarCouta.setBounds(47, 173, 117, 23);
		contentPane.add(btnPagarCouta);
		
		JButton btnActualizarDatos = new JButton("Actualizar Datos");
		btnActualizarDatos.setBounds(47, 207, 117, 23);
		contentPane.add(btnActualizarDatos);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.setBounds(47, 241, 117, 23);
		contentPane.add(btnSalir);
		
		JLabel lblSeleccioneUnaOpcin = new JLabel("Seleccione una opción");
		lblSeleccioneUnaOpcin.setBounds(47, 32, 117, 14);
		contentPane.add(lblSeleccioneUnaOpcin);
	}

}
