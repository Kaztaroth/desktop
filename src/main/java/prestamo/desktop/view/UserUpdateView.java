package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import prestamo.desktop.model.RegionModel;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class UserUpdateView extends JFrame {

	public JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	public JComboBox comboBox;
	
	public static void main() {
		UserUpdateView frame = new UserUpdateView();
		frame.setVisible(true);
		
	}


	/**
	 * Create the frame.
	 */
	public UserUpdateView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 460, 407);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("New menu");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmInicio = new JMenuItem("Inicio");
		mnNewMenu.add(mntmInicio);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblActualizarTusDatos = new JLabel("Actualizar tus datos");
		lblActualizarTusDatos.setBounds(173, 11, 107, 14);
		contentPane.add(lblActualizarTusDatos);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(59, 78, 46, 14);
		contentPane.add(lblNombre);
		
		textField = new JTextField();
		textField.setBounds(169, 75, 236, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(173, 106, 232, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(59, 109, 46, 14);
		contentPane.add(lblApellido);
	
		
		
		
		
		JLabel lblRegiones = new JLabel("Regiones");
		lblRegiones.setBounds(59, 152, 46, 14);
		contentPane.add(lblRegiones);
		
		
		
	}
}
