package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;

public class prestamoView extends JFrame {

	private JPanel contentPane;
	private JTextField txtMontoCuota;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JButton btnGuardar;
	private JButton btnCancelar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					prestamoView frame = new prestamoView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public prestamoView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPrstamo = new JLabel("Pr\u00E9stamo");
		lblPrstamo.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblPrstamo.setBounds(58, 28, 68, 14);
		contentPane.add(lblPrstamo);
		
		JLabel lblMotivo = new JLabel("Motivo :");
		lblMotivo.setBounds(58, 69, 46, 14);
		contentPane.add(lblMotivo);
		
		JLabel lblNewLabel = new JLabel("Monto :");
		lblNewLabel.setBounds(58, 141, 46, 14);
		contentPane.add(lblNewLabel);
		
		txtMontoCuota = new JTextField();
		txtMontoCuota.setBounds(174, 66, 86, 20);
		contentPane.add(txtMontoCuota);
		txtMontoCuota.setColumns(10);
		
		JLabel lblMontoCuota = new JLabel("Monto Cuota :");
		lblMontoCuota.setBounds(56, 173, 86, 20);
		contentPane.add(lblMontoCuota);
		
		JLabel lblRut = new JLabel("RUT :");
		lblRut.setBounds(58, 104, 46, 14);
		contentPane.add(lblRut);
		
		textField = new JTextField();
		textField.setBounds(174, 138, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(174, 173, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(174, 101, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(235, 219, 89, 23);
		contentPane.add(btnGuardar);
		
		btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(100, 219, 89, 23);
		contentPane.add(btnCancelar);
	}

}
