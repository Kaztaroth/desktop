package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.JList;
import javax.swing.JButton;

public class CuotaView extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CuotaView frame = new CuotaView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CuotaView() {
		setTitle("Estado Prestamo");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 294, 282);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMontoPrestamo = new JLabel("Monto prestamo");
		lblMontoPrestamo.setBounds(46, 43, 87, 14);
		contentPane.add(lblMontoPrestamo);
		
		JLabel label = new JLabel("5.000.000");
		label.setBounds(179, 43, 76, 14);
		contentPane.add(label);
		
		JLabel lblCantidadDeCuotas = new JLabel("Cantidad de cuotas");
		lblCantidadDeCuotas.setBounds(46, 68, 99, 14);
		contentPane.add(lblCantidadDeCuotas);
		
		JLabel label_1 = new JLabel("12");
		label_1.setBounds(176, 68, 46, 14);
		contentPane.add(label_1);
		
		JLabel lblConsultarPorCuota = new JLabel("Consultar por cuota");
		lblConsultarPorCuota.setBounds(46, 93, 99, 14);
		contentPane.add(lblConsultarPorCuota);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(179, 90, 29, 20);
		contentPane.add(spinner);
		
		JLabel lblEstadoCuota = new JLabel("Estado Cuota");
		lblEstadoCuota.setBounds(46, 118, 65, 14);
		contentPane.add(lblEstadoCuota);
		
		JList list = new JList();
		list.setBounds(179, 121, 65, 14);
		contentPane.add(list);
		
		JLabel lblMontoCuota = new JLabel("Monto Cuota");
		lblMontoCuota.setBounds(46, 143, 87, 14);
		contentPane.add(lblMontoCuota);
		
		JLabel label_2 = new JLabel("55.000");
		label_2.setBounds(176, 146, 46, 14);
		contentPane.add(label_2);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.setBounds(101, 195, 89, 23);
		contentPane.add(btnVolver);
	}

}
