package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import prestamo.desktop.model.UserModel;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import java.awt.Label;

public class MenuView extends JFrame implements View {

	private static MenuView instancia;
	private JPanel contentPane;
	private JMenuBar menuBar;
	private JMenu mnPrestamos;
	private JMenuItem mntmSolicitar;
	private JMenuItem mntmPrestamos;
	private Label label;
	
	/**
	 * Launch the application.
	 */
	public static MenuView getMenuView() {
		if(instancia == null) {
			instancia = new MenuView();
		}
		return instancia;
	}
	
	/**
	 * Create the frame.
	 */
	public MenuView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnPrestamos = new JMenu("Prestamos");
		menuBar.add(mntmPrestamos);
		
		JMenuItem mntmSolicitar = new JMenuItem("Solicitar");
		mnPrestamos.add(mntmSolicitar);
		
		JMenuItem mntmPrestamos = new JMenuItem("Prestamos");
		mnPrestamos.add(mntmPrestamos);
		
		Label label = new Label("Bienvenido");
		contentPane.add(label, BorderLayout.WEST);
		
	}
	
	public JMenuItem getMntmSolicitar() {
		return mntmSolicitar;
	}
	
	public void setMntmSolicitar(JMenuItem mntmSolicitar) {
		this.mntmSolicitar = mntmSolicitar;
	}
	
	public JMenuItem getMntmPrestamos() {
		return mntmPrestamos;
	}
	
	public void setMntmPrestamos(JMenuItem mntmPrestamos) {
		this.mntmPrestamos = mntmPrestamos;
	}

}
