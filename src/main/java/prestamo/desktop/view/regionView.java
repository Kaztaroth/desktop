package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import javax.swing.JComboBox;
import javax.swing.JTree;
import javax.swing.JSpinner;
import javax.swing.JToggleButton;
import javax.swing.JScrollBar;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JLabel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.UIManager;
import java.awt.SystemColor;
import java.awt.Color;

public class regionView extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					regionView frame = new regionView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public regionView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblRegin = new JLabel("Región :");
		lblRegin.setBounds(99, 115, 46, 14);
		contentPane.add(lblRegin);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setToolTipText("Seleccione una región");
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"I de Tarapacá", "II de Antofagasta", "III de Atacama", "IV de Coquimbo", "V de Valparaíso", "VI del Libertador General Bernardo O'Higgins", "VII del Maule", "VIII de Concepción", "IX de la Araucanía", "X de Los Lagos", "XI de Aysén del General Carlos Ibañez del Campo", "XII de Magallanes y de la Antártica Chilena", "Metropolitana", "XIV de Los Ríos", "XV de Arica y Parinacota", "XVI de Ñuble"}));
		comboBox.setBounds(155, 107, 184, 31);
		contentPane.add(comboBox);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setBackground(SystemColor.controlShadow);
		btnGuardar.setForeground(SystemColor.desktop);
		btnGuardar.setBounds(195, 186, 71, 23);
		contentPane.add(btnGuardar);
	}
}
