package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;

public class ComunaView extends JFrame {

	private JPanel contentPane;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ComunaView frame = new ComunaView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ComunaView() {
		setTitle("Registro Nueva Comuna");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 314, 283);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNombreComuna = new JLabel("Nombre Comuna");
		lblNombreComuna.setBounds(57, 84, 89, 14);
		contentPane.add(lblNombreComuna);
		
		textField = new JTextField();
		textField.setBounds(156, 81, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(156, 112, 86, 20);
		contentPane.add(comboBox);
		
		JLabel lblRegion = new JLabel("Region");
		lblRegion.setBounds(57, 115, 46, 14);
		contentPane.add(lblRegion);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(112, 170, 89, 23);
		contentPane.add(btnGuardar);
		
		JLabel lblIngreseNuevaComuna = new JLabel("Ingrese Nueva Comuna");
		lblIngreseNuevaComuna.setBounds(90, 45, 120, 14);
		contentPane.add(lblIngreseNuevaComuna);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(112, 204, 89, 23);
		contentPane.add(btnCancelar);
	}
}
