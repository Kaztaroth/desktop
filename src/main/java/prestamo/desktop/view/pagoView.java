package prestamo.desktop.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;

public class pagoView extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					pagoView frame = new pagoView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public pagoView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblPago = new JLabel("Pago");
		lblPago.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblPago.setBounds(50, 32, 46, 14);
		contentPane.add(lblPago);
		
		textField = new JTextField();
		textField.setBounds(172, 77, 125, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblMonto = new JLabel("Monto :");
		lblMonto.setBounds(66, 80, 46, 14);
		contentPane.add(lblMonto);
		
		JLabel lblRut = new JLabel("Rut :");
		lblRut.setBounds(66, 126, 46, 14);
		contentPane.add(lblRut);
		
		textField_1 = new JTextField();
		textField_1.setBounds(172, 123, 125, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblCdigoPrstamo = new JLabel("Código Préstamo :");
		lblCdigoPrstamo.setBounds(66, 170, 97, 14);
		contentPane.add(lblCdigoPrstamo);
		
		textField_2 = new JTextField();
		textField_2.setBounds(172, 167, 97, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JButton btnNewButton = new JButton("Guardar");
		btnNewButton.setBounds(172, 213, 89, 23);
		contentPane.add(btnNewButton);
	}
}
