package prestamo.desktop.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserModel implements Model {

	private static UserModel instancia;
	private String id;
	private String nombre;
	private String apellido;
	private String rut;
	private String comunaId;
	private String calle;
	private String numeroCasa;
	private String correo;
	private String tipo;
	private String reputacionId;
	private String clave;
		
	private UserModel() {
		
	}
	
	public static UserModel getUserModel() {
		if(instancia==null) {
			instancia = new UserModel();
		}
		return instancia;
	}
	
	@Override
	public boolean create() {
		// TODO Auto-generated method stub
		Connection con = DB.getConnection();
		boolean resultado = false;
		String sql = "INSERT INTO Usuario("
				+ "nombre,"
				+ "apellido,"
				+ "rut,"
				+ "correo,"
				+ "tipo,"
				+ "calle,"
				+ "numerocasa"
				+ "reputacion_id,"
				+ "comuna_id,"
				+ "clave) VALUES(?,?,?,?,?,?,?,?,?)";
		
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1,this.nombre);
			ps.setString(2,this.apellido);
			ps.setString(3,this.rut);
			ps.setString(4,this.correo);
			ps.setString(5,this.tipo);
			ps.setString(6,this.calle);
			ps.setString(7,this.numeroCasa);
			ps.setString(8,this.comunaId);
			ps.setString(9,this.reputacionId);
			resultado = ps.execute();			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return resultado;
	}

	@Override
	public boolean update() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean get() {
		// TODO Auto-generated method stub
		return false;
	}

	
	/**
	 * Método Autenticación
	 */
	public boolean autenticate(String user, String pass) {
		
		Connection con = DB.getConnection();
		boolean resultado = false;
		
		String sql = "SELECT * FROM "
				+ "Usuario WHERE "
				+ "correo = ? AND clave = ? ;";
		
			try {
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, user);
				ps.setString(2, pass);
				ResultSet rs = ps.executeQuery();
				
				while(rs.next()) {
					this.id = rs.getString("id");
					this.nombre = rs.getString("nombre");
					this.apellido = rs.getString("apellido");
					this.rut = rs.getString("rut");
					this.correo = rs.getString("correo");
					this.tipo = rs.getString("tipo");
					this.calle = rs.getString("calle");
					this.numeroCasa = rs.getString("numerocasa");
					this.reputacionId  = rs.getString("reputacion_id");
					this.comunaId = rs.getString("comuna_id");
				}
				resultado = true;

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				con.close();
			}
			catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return resultado;
				
	}
	
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getComuna() {
		return comunaId;
	}

	public void setComuna(String comunaId) {
		this.comunaId = comunaId;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumeroCasa() {
		return numeroCasa;
	}

	public void setNumeroCasa(String numeroCasa) {
		this.numeroCasa = numeroCasa;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getReputacionId() {
		return reputacionId;
	}

	public void setReputacionId(String reputacionId) {
		this.reputacionId = reputacionId;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

		
}


	
