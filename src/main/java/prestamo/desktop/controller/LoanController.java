package prestamo.desktop.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import prestamo.desktop.model.LoanModel;
import prestamo.desktop.view.LoanView;

public class LoanController implements ActionListener{

	private LoanView loanView;
	private LoanModel loanModel;
	public LoanController(LoanView loanView, LoanModel loanModel){
		this.loanView = loanView;
		this.loanModel = loanModel;
		this.loanView.getBtnModify().addActionListener(this);
		this.loanView.getBtnDelete().addActionListener(this);
		this.loanView.getBtnRegister().addActionListener(this);
		this.loanView.getBtnSearch().addActionListener(this);
	}
	
	public void iniciar() {
		loanView.setTitle("Prestamos");
		loanView.setVisible(true);
	}
	
	@Override
	public void actionPerformed(ActionEvent ae) {
		if(ae.getSource()==loanView.getBtnRegister()) {
			
			loanModel.setNumber(loanView.getNumberField().toString());
			loanModel.setPerson(loanView.getPersonField().toString());
//			loanModel.setAmount(Double.valueOf(loanView.getAmountField().toString()));
//			loanModel.setQuota(Integer.valueOf(loanView.getQuotaComboBox().toString()));
			
			
//			this.limpiar();
		}else if(ae.getSource()==loanView.getBtnModify()) {
			
			loanModel.setNumber(loanView.getNumberField().toString());
			loanModel.setPerson(loanView.getPersonField().toString());
			loanModel.setAmount(Double.valueOf(loanView.getAmountField().toString()));
			this.limpiar();
		}else if(ae.getSource()==loanView.getBtnDelete()) {
			
			loanModel.setNumber(loanView.getNumberField().toString());
			loanModel.setPerson(loanView.getPersonField().toString());
			loanModel.setAmount(Double.valueOf(loanView.getAmountField().toString()));
			this.limpiar();
		}else if(ae.getSource()==loanView.getBtnSearch()) {

			loanModel.setNumber(loanView.getNumberField().toString());
			loanModel.setPerson(loanView.getPersonField().toString());
			loanModel.setAmount(Double.valueOf(loanView.getAmountField().toString()));
			this.limpiar();
		}
	}

	public void limpiar(){
		
		loanView.setNumberField(null);
		loanView.setPersonField(null);
		loanView.setAmountField(null);
		
	}
}
